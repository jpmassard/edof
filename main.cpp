// +-----------------------------------------------------------------------+
// | edof - Enhanced depth of field _ 3D photo                             |
// | Converts a picture taken in depth of field mode with a two sensors    |
// | Honor (6x) camera in a set of two jpeg files of same dimensions :     |
// |   - the jpeg image                                                    |
// |   - the depth map                                                     |
// | Each of them can be be edited with conventional tools before 3D       |
// | rendering with StereoPhoto Maker                                      |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2014-2017 Jean-Paul MASSARD                              |
// +-----------------------------------------------------------------------+
// | Version 0.1 : alpha with basic fonctionnalities                                  |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+

#include <QFile>
#include <QByteArray>
#include <QDebug>
#include <QImage>

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        qDebug () << "Syntaxe: " << argv[0] << " <infile>";
        return -1;
    }
    QFile in(argv[1]);
    if (!in.open (QIODevice::ReadOnly))
    {
        qDebug () << "Cannot open input file";
        return -2;
    }
    QByteArray b=in.readAll();
    in.close();

    // Ignore first image
    QByteArray bPattern;
    bPattern.push_back(0xff);
    bPattern.push_back(0xd8);
    bPattern.push_back(0xff);
    bPattern.push_back(0xe1);
    int pos= b.lastIndexOf(bPattern);
    QByteArray imgDep = b.mid(pos);
    b.clear();

    // find img and depth data
    bPattern.clear();
    bPattern.push_back(0xff);
    bPattern.push_back(0xd9);
    pos= imgDep.lastIndexOf(bPattern);
    QByteArray img = imgDep.left(pos+2);
    QByteArray depth = imgDep.mid(pos+2);

    // Write image file
    QFile out(QString("I_") + argv[1]);
    if (!out.open (QIODevice::WriteOnly))
    {
        qDebug () << "cannot create image file";
        return -3;
    }
    out.write(img);
    out.close();

    // find initial image width and height
    bPattern.clear();
    bPattern.push_back(0xff);
    bPattern.push_back(0xc0);
    pos= img.lastIndexOf(bPattern);
    int iHeight= img[pos+6] + (img[pos+5]<<8);
    int iWidth= img[pos+8] + (img[pos+7]<<8);
    img.clear();

    // Get original depth image
    int dWidth= depth[0x18] + (depth[0x19]<<8);
    int dHeight= depth[0x1a] + (depth[0x1b]<<8);
    QByteArray data = depth.mid(0x4c, dWidth*dHeight);
    QImage iDepth((uchar*)data.data(), dWidth, dHeight, dWidth, QImage::Format_Grayscale8);

    // resample so image and depth have same size
    QImage iDepthScaled = iDepth.scaled(iWidth, iHeight);
    iDepthScaled.save(QString("D_") + argv[1]);
    return 0;
}
